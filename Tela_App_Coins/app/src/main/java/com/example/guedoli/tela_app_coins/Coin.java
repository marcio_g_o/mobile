package com.example.guedoli.tela_app_coins;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class Coin {

    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("symbol")
    public String symbol;

    @SerializedName("rank")
    public String rank;

    @SerializedName("price_usd")
    public String priceUsd;

    @SerializedName("price_btc")
    public String priceBtc;

    @SerializedName("percent_change_1h")
    public String percentChange1h;

    @SerializedName("percent_change_24h")
    public String percentChange24h;

    @SerializedName("percent_change_7d")
    public String percentChange7d;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBitcoin() {
        return name;
    }

    public void setName(String bitcoin) {
        this.name = bitcoin;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getPriceUsd() {
        return priceUsd;
    }

    public void setPriceUsd(String priceUsd) {
        this.priceUsd = priceUsd;
    }

    public String getPriceBtc() {
        return priceBtc;
    }

    public void setPriceBtc(String priceBtc) {
        this.priceBtc = priceBtc;
    }

    public String getPercentChange1h() {
        return percentChange1h;
    }

    public void setPercentChange1h(String percentChange1h) {
        this.percentChange1h = percentChange1h;
    }

    public String getPercentChange24h() {
        return percentChange24h;
    }

    public void setPercentChange24h(String percentChange24h) {
        this.percentChange24h = percentChange24h;
    }

    public String getPercentChange7d() {
        return percentChange7d;
    }

    public void setPercentChange7d(String percentChange7d) {
        this.percentChange7d = percentChange7d;
    }

}
