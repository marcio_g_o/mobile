package com.example.guedoli.tela_app_coins;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity { //app de compatibilidade para os devices

    RecyclerView rvCoins;
    CoinAdapter coinAdapter;
    List<Coin> coins;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvCoins = findViewById(R.id.rv_coins);
        coins = new ArrayList<>();
        context = this;
        coinAdapter = new CoinAdapter(context, coins);
        rvCoins.setLayoutManager(new LinearLayoutManager(   context,
                LinearLayoutManager.VERTICAL, false)); //Gerenciar layout via código
        rvCoins.setAdapter(coinAdapter); //Set o layout que será usado

    }
}
